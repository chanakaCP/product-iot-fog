/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.analytics;

import io.entgra.iot.fog.analytics.beans.SiddhiQuery;
import io.entgra.iot.fog.analytics.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.siddhi.core.SiddhiAppRuntime;
import org.wso2.siddhi.core.SiddhiManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SiddhiEngine {

    private static final Logger log = LogManager.getLogger(SiddhiEngine.class);

    private SiddhiManager siddhiManager;
    private Map<String, SiddhiAppRuntime> siddhiAppRuntimes = new HashMap<>();

    public SiddhiEngine() throws IOException {
        siddhiManager = new SiddhiManager();
        startEngine();
    }

    public void startEngine() throws IOException {
        Utils.getAllQueries().forEach(siddhiQuery -> {
            try {
                SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiQuery.getContent());
                siddhiAppRuntime.start();
                siddhiAppRuntimes.put(siddhiQuery.getName(), siddhiAppRuntime);
            } catch (Exception e) {
                log.error("Error occurred while starting siddhi runtime for query: " + siddhiQuery.getName(), e);
            }
        });
    }

    public void deployQuery(SiddhiQuery siddhiQuery) {
        SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiQuery.getContent());
        siddhiAppRuntime.start();
        siddhiAppRuntimes.put(siddhiQuery.getName(), siddhiAppRuntime);
    }

    public void redeployQuery(SiddhiQuery siddhiQuery) {
        SiddhiAppRuntime siddhiAppRuntime = siddhiAppRuntimes.get(siddhiQuery.getName());
        siddhiAppRuntime.shutdown();
        siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiQuery.getContent());
        siddhiAppRuntime.start();
        siddhiAppRuntimes.put(siddhiQuery.getName(), siddhiAppRuntime);
    }

    public void unDeployQuery(String qName) {
        SiddhiAppRuntime siddhiAppRuntime = siddhiAppRuntimes.get(qName);
        siddhiAppRuntime.shutdown();
        siddhiAppRuntimes.remove(qName);
    }

}
