/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.utils;

import io.entgra.iot.fog.usermgt.beans.AuthenticationInfo;
import io.entgra.iot.fog.usermgt.beans.AuthenticationStatus;
import io.entgra.iot.fog.usermgt.beans.User;
import io.entgra.iot.fog.usermgt.beans.UserCredential;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAO;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAOFactory;
import io.entgra.iot.fog.usermgt.dao.UserManagementGenericDAOImpl;
import io.entgra.iot.fog.usermgt.exceptions.AuthenticationException;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;
import org.apache.axiom.om.util.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class CommonUtil {

    private static final Logger log = LogManager.getLogger(CommonUtil.class);
    private static final String SHA_1_PRNG = "SHA1PRNG";

    /**
     * Prepare the password including the salt, and hashes if hash algorithm is provided
     *
     * @param password  original password value
     * @param saltValue salt value
     * @return hashed password or plain text password as a String
     * @throws UserManagementException if unable to prepare password
     */
    public static String preparePassword(Object password, String saltValue) throws UserManagementException {

        Secret credentialObj;
        try {
            credentialObj = Secret.getSecret(password);
        } catch (Exception e) {
            throw new UserManagementException("Unsupported credential type", e);
        }

        try {
            String passwordString;
            if (saltValue != null) {
                credentialObj.addChars(saltValue.toCharArray());
            }

            String digestFunction = "SHA-256";
            if (digestFunction != null) {
                if (digestFunction.equals("PLAIN_TEXT")) {
                    passwordString = new String(credentialObj.getChars());
                    return passwordString;
                }

                MessageDigest digest = MessageDigest.getInstance(digestFunction);
                byte[] byteValue = digest.digest(credentialObj.getBytes());
                passwordString = Base64.encode(byteValue);
            } else {
                passwordString = new String(credentialObj.getChars());
            }

            return passwordString;
        } catch (NoSuchAlgorithmException e) {
            String msg = "Error occurred while preparing password.";
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new UserManagementException(msg, e);
        } finally {
            credentialObj.clear();
        }
    }

    public static UserCredential parseBasicAuthHeader(String authHeader) throws AuthenticationException {
        UserCredential credential = new UserCredential();
        if (authHeader != null) {
            String authType = authHeader.substring(0, Constants.AUTH_TYPE_BASIC_LENGTH);
            String authEncoded = authHeader.substring(Constants.AUTH_TYPE_BASIC_LENGTH).trim();
            if (Constants.AUTH_TYPE_BASIC.equals(authType) && !authEncoded.isEmpty()) {
                byte[] decodedByte = authEncoded.getBytes(Charset.forName(Constants.CHARSET_UTF_8));
                String authDecoded = new String(java.util.Base64.getDecoder().decode(decodedByte),
                        Charset.forName(Constants.CHARSET_UTF_8));
                String[] decodedCredentials = authDecoded.split(":");
                credential.setUsername(decodedCredentials[0]);
                credential.setPassword(decodedCredentials[1]);
            }
        }

        return credential;
    }

    public static String generateSaltValue() {
        String saltValue;
        try {
            SecureRandom secureRandom = SecureRandom.getInstance(SHA_1_PRNG);
            byte[] bytes = new byte[16];
            //secureRandom is automatically seeded by calling nextBytes
            secureRandom.nextBytes(bytes);
            saltValue = Base64.encode(bytes);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("SHA1PRNG algorithm could not be found.");
        }
        return saltValue;
    }

    /**
     * @param dbConnection
     * @param sqlStmt
     * @param params
     * @throws Exception
     */
    public static void updateStringValuesToDatabase(Connection dbConnection, String sqlStmt, Object... params)
            throws Exception {
        PreparedStatement prepStmt = null;
        boolean localConnection = false;
        try {
            if (dbConnection == null) {
                localConnection = true;
                dbConnection = DataBaseUtil.getDBConnection();
            }
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    Object param = params[i];
                    if (param == null) {
                        throw new Exception("Invalid data provided");
                    } else if (param instanceof String) {
                        prepStmt.setString(i + 1, (String) param);
                    } else if (param instanceof Integer) {
                        prepStmt.setInt(i + 1, (Integer) param);
                    } else if (param instanceof Date) {
                        // Timestamp timestamp = new Timestamp(((Date) param).getTime());
                        // prepStmt.setTimestamp(i + 1, timestamp);
                        prepStmt.setTimestamp(i + 1, new Timestamp(System.currentTimeMillis()));
                    } else if (param instanceof Boolean) {
                        prepStmt.setBoolean(i + 1, (Boolean) param);
                    }
                }
            }
            int count = prepStmt.executeUpdate();

            if (log.isDebugEnabled()) {
                if (count == 0) {
                    log.debug("No rows were updated");
                }
                log.debug("Executed query is " + sqlStmt + " and number of updated rows :: " + count);
            }

            if (localConnection) {
                dbConnection.commit();
            }
        } catch (SQLException e) {
            String msg = "Error occurred while updating string values to database.";
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new Exception(msg, e);
        } finally {
            if (localConnection) {
                DataBaseUtil.closeAllConnections(dbConnection);
            }
            DataBaseUtil.closeAllConnections(null, prepStmt);
        }
    }

    /**
     * Prepare the batch
     *
     * @param prepStmt
     * @param params
     * @throws Exception
     */
    public static void batchUpdateStringValuesToDatabase(PreparedStatement prepStmt, Object... params) throws Exception {
        try {
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    Object param = params[i];
                    if (param == null) {
                        throw new Exception("Invalid data provided");
                    } else if (param instanceof String) {
                        prepStmt.setString(i + 1, (String) param);
                    } else if (param instanceof Integer) {
                        prepStmt.setInt(i + 1, (Integer) param);
                    } else if (param instanceof Date) {
                        prepStmt.setTimestamp(i + 1, new Timestamp(System.currentTimeMillis()));
                    } else if (param instanceof Boolean) {
                        prepStmt.setBoolean(i + 1, (Boolean) param);
                    }
                }
            }
            prepStmt.addBatch();
        } catch (SQLException e) {
            String msg = "Error occurred while updating property values to database.";
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new Exception(msg, e);
        }
    }

    /**
     * This method reads a given CSV stream and stored users
     * @param inStream CSV stream
     * @return returns failed list of records
     * @throws IOException
     */
    public static List<String> importUsersFromCSV(InputStream inStream) throws IOException {
        BufferedReader reader = null;
        List<String> failedRecords = new ArrayList<>();
        try {
            reader = new BufferedReader(new InputStreamReader(inStream));
            UserManagementDAO userManagementDAO = UserManagementGenericDAOImpl.getUserManagementDAO();
            reader.readLine(); // this is just to skip the first line
            User user;
            for (String line; (line = reader.readLine()) != null; ) {
                try {
                    user = parseUser(line);
                    userManagementDAO.addUser(user);
                    if (log.isDebugEnabled()) {
                        log.debug(user.getUserName() + "added");
                    }
                } catch (Throwable e) { // adding throwable to catch all exceptions
                    log.error("Failed to add record '" + line + "' from CSV due to invalid format", e);
                    failedRecords.add(line);
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return failedRecords;
    }

    /*
     * This method returns an user instance for a given csv line.
     * Derivation of the csv as below.
     * [0] Emp. No.
     * [1] Name
     * [2] Service Phone
     * [3] DID
     * [4] UID
     *
     * In order to create the user, user name has to be extracted from [1] Name by splitting it using "-"
     */
    private static User parseUser(String line) {
        Map<String, String> claims = new HashMap<>();
        String values[] = line.split(",");
        String names[] = values[1].split("-");
        User user = new User("0" + values[4].trim().toLowerCase(), "1234"); // setting UID as user name
        user.setRoles(new String[]{Constants.USER_ROLE_NAME});
        claims.put(Constants.CLAIM_FULL_NAME, names[1].trim());
        claims.put(Constants.CLAIM_MOBILE, values[2].trim());
        user.setClaims(claims);
        return user;
    }

    public static AuthenticationInfo getAuthenticationInfo(UserCredential credential, boolean isAuthenticated)
            throws UserManagementException {
        AuthenticationInfo authenticationInfo = new AuthenticationInfo();
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        if (isAuthenticated) {
            authenticationInfo.setStatus(AuthenticationStatus.SUCCESS);
            authenticationInfo.setUser(userManagementDAO.getUser(credential.getUsername()));
        } else {
            User user = new User();
            user.setUserName(credential.getUsername());
            if (userManagementDAO.isUserExist(credential.getUsername())) {
                authenticationInfo.setStatus(AuthenticationStatus.INVALID_PASSWORD);
            } else {
                authenticationInfo.setStatus(AuthenticationStatus.INVALID_USER);
            }
            authenticationInfo.setUser(user);
            authenticationInfo.setMessage("Failed to authorize incoming request.");
        }
        return authenticationInfo;
    }
}
