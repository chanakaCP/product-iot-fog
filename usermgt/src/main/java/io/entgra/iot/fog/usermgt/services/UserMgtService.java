/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.services;

import io.entgra.iot.fog.usermgt.beans.AuthenticationInfo;
import io.entgra.iot.fog.usermgt.beans.User;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAO;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAOFactory;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;
import io.entgra.iot.fog.usermgt.interceptor.BasicAuthRequestInterceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.msf4j.Request;
import org.wso2.msf4j.interceptor.annotation.RequestInterceptor;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

/**
 * This is the Microservice resource class.
 * See <a href="https://github.com/wso2/msf4j#getting-started">https://github.com/wso2/msf4j#getting-started</a>
 * for the usage of annotations.
 *
 * @since 0.9.0-SNAPSHOT
 */
@Path("/usermgt/user/{userName}")
@RequestInterceptor(BasicAuthRequestInterceptor.class)
public class UserMgtService {

    private static final Logger log = LogManager.getLogger(BasicAuthRequestInterceptor.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@Context Request request, @PathParam("userName") String userName) {
        AuthenticationInfo authenticationInfo = (AuthenticationInfo) request.getProperty("authenticationInfo");
        User authenticatedUser = authenticationInfo.getUser();

        if (Arrays.asList(authenticatedUser.getRoles()).contains("admin") ||
                Arrays.asList(authenticatedUser.getRoles()).contains("system")) {
            UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
            try {
                User user = userManagementDAO.getUser(userName);
                if (user != null) {
                    return Response.ok().entity(user).build();
                } else {
                    return Response.status(Response.Status.NOT_FOUND).entity("User " + userName + " not found.").build();
                }
            } catch (UserManagementException e) {
                log.error(e.getMessage(), e);
                return Response.serverError().entity(e.getMessage()).build();
            }
        } else if (authenticatedUser.getUserName().equals(userName)) {
            return Response.ok().entity(authenticatedUser).build();
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(@Context Request request, @PathParam("userName") String userName, User modifiedUser) {
        AuthenticationInfo authenticationInfo = (AuthenticationInfo) request.getProperty("authenticationInfo");
        User authenticatedUser = authenticationInfo.getUser();

        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        try {
            User user = userManagementDAO.getUser(userName);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("User " + userName + " not found.").build();
            }
        } catch (UserManagementException e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }

        if (Arrays.asList(authenticatedUser.getRoles()).contains("admin") || authenticatedUser.getUserName()
                .equals(userName)) {
            modifiedUser.setPassword(null);
            try {
                userManagementDAO.updateUser(modifiedUser);
            } catch (UserManagementException e) {
                log.error(e.getMessage(), e);
                return Response.serverError().entity(e.getMessage()).build();
            }
            return Response.accepted().build();
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @PUT
    @Path("/change-password")
    public Response put(@Context Request request, @PathParam("userName") String userName, String newPassword) {
        AuthenticationInfo authenticationInfo = (AuthenticationInfo) request.getProperty("authenticationInfo");
        User authenticatedUser = authenticationInfo.getUser();
        authenticatedUser.setPassword(newPassword);
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        try {
            userManagementDAO.updateUser(authenticatedUser);
        } catch (UserManagementException e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
        return Response.accepted().build();
    }
}
