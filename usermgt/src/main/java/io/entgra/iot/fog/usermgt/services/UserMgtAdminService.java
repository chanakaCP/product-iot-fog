/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.services;

import io.entgra.iot.fog.usermgt.beans.User;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAO;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAOFactory;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;
import io.entgra.iot.fog.usermgt.interceptor.BasicAuthAdminRequestInterceptor;
import io.entgra.iot.fog.usermgt.utils.CommonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.interceptor.annotation.RequestInterceptor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is the Microservice resource class.
 * See <a href="https://github.com/wso2/msf4j#getting-started">https://github.com/wso2/msf4j#getting-started</a>
 * for the usage of annotations.
 *
 * @since 0.9.0-SNAPSHOT
 */
@Path("/usermgt")
@RequestInterceptor(BasicAuthAdminRequestInterceptor.class)
public class UserMgtAdminService {

    private static final Logger log = LogManager.getLogger(BasicAuthAdminRequestInterceptor.class);

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        List<User> userList;
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        try {
            userList = userManagementDAO.getAllUsers();
        } catch (UserManagementException e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
        return Response.ok().entity(userList).build();
    }

    @GET
    @Path("/{role}/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@PathParam("userName") String role) {
        List<User> userList;
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        try {
            userList = userManagementDAO.getUsers(role);
        } catch (UserManagementException e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
        return Response.ok().entity(userList).build();
    }

    @POST
    @Path("/user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response post(User user) {
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        try {
            if (userManagementDAO.isUserExist(user.getUserName())) {
                return Response.status(Response.Status.CONFLICT).
                        entity("User '" + user.getUserName() + "' already exists").build();
            }
            userManagementDAO.addUser(user);
            return Response.created(new URI("/usermgt/user/" + user.getUserName())).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/user/{userName}")
    public Response delete(@PathParam("userName") String userName) {
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();
        try {
            if(userManagementDAO.isUserExist(userName)) {
                userManagementDAO.deleteUser(userName);
            } else {
                return Response.status(Response.Status.NOT_FOUND).entity("User: " + userName + "not found.").build();
            }
        } catch (UserManagementException e) {
            return Response.serverError().entity(e.getMessage()).build();
        } return Response.accepted().build();
    }

    @PUT
    @Path("/user/{userName}/reset-password")
    public Response put(@PathParam("userName") String userName, String newPassword) {

        if (userName == null || userName.isEmpty() || newPassword == null || newPassword.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Username/Password is unavailable").build();
        }
        UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();

        try {
            User user = userManagementDAO.getUser(userName);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("'" + userName + "' not found").build();
            }
            user = new User(userName, newPassword);
            userManagementDAO.updateUser(user);
            return Response.accepted().build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/users/import")
    public Response post(@FormDataParam("file") InputStream uploadedInputStream) {
        try {
            if (uploadedInputStream == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Invalid form data").build();
            }
            List<String> failedRecords = CommonUtil.importUsersFromCSV(uploadedInputStream);
            if (failedRecords.size() == 0) {
                return Response.created(new URI("/usermgt/users/import")).build();
            } else {
                String msg = failedRecords.stream().collect(Collectors.joining("\n"));
                return Response.status(Response.Status.CREATED).entity("Failed records: \n" + msg +
                        "\nPlease check the format of the records in given csv file").build();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

}
