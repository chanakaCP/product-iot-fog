/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt;

import io.entgra.iot.fog.usermgt.services.AuthenticationService;
import io.entgra.iot.fog.usermgt.services.UserMgtAdminService;
import io.entgra.iot.fog.usermgt.services.UserMgtClaimService;
import io.entgra.iot.fog.usermgt.services.UserMgtService;
import io.entgra.iot.fog.usermgt.utils.DataBaseUtil;
import io.entgra.iot.fog.usermgt.utils.DatabaseCreator;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.msf4j.MicroservicesRunner;

/**
 * Application entry point.
 *
 * @since 0.9.0-SNAPSHOT
 */
public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        BasicDataSource dataSource = new BasicDataSource();
        String connectionUrl = "jdbc:h2:~/iot-fog/UM_DB";
        dataSource.setUrl(connectionUrl);
        dataSource.setDriverClassName("org.h2.Driver");

        DatabaseCreator dbCreator = new DatabaseCreator(dataSource);
        try {
            dbCreator.createDatabase();
            DataBaseUtil.createDefaultUserAndRole();
        } catch (Exception e) {
            log.error("Error while creating usermgt tables!", e);
        }

        new MicroservicesRunner(9091)
                .deploy(new UserMgtService(), new UserMgtAdminService(), new UserMgtClaimService(), new AuthenticationService())
                .start();
    }
}
