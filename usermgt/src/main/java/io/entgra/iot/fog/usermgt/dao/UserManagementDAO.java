/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.dao;

import io.entgra.iot.fog.usermgt.beans.User;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;

import java.sql.SQLException;
import java.util.List;

public interface UserManagementDAO {

    void addUser(User user) throws UserManagementException;

    User getUser(String username) throws UserManagementException;

    String getUserName(String claim) throws UserManagementException;

    List<User> getUsers(String role) throws UserManagementException;

    List<User> getAllUsers() throws UserManagementException;

    void updateUser(User user) throws UserManagementException;

    void deleteUser(String username) throws UserManagementException;

    void addRole(String roleName, boolean sharedRole) throws UserManagementException, SQLException;

    boolean isUserExist(String username) throws UserManagementException;
}
