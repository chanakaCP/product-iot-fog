/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.utils;

import io.entgra.iot.fog.usermgt.beans.User;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAO;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAOFactory;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DataBaseUtil {
    private static final Logger log = LogManager.getLogger(DataBaseUtil.class);

    private static void closeConnection(Connection dbConnection) {

        if (dbConnection != null) {
            try {
                dbConnection.close();
            } catch (SQLException e) {
                log.error("Database error. Could not close statement. Continuing with others. - " + e.getMessage(), e);
            }
        }
    }

    private static void closeResultSet(ResultSet rs) {

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error("Database error. Could not close result set  - " + e.getMessage(), e);
            }
        }

    }

    private static void closeStatement(PreparedStatement preparedStatement) {

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.error("Database error. Could not close statement. Continuing with others. - " + e.getMessage(), e);
            }
        }

    }

    private static void closeStatements(PreparedStatement... prepStmts) {

        if (prepStmts != null && prepStmts.length > 0) {
            for (PreparedStatement stmt : prepStmts) {
                closeStatement(stmt);
            }
        }

    }

    public static void closeAllConnections(Connection dbConnection, PreparedStatement... prepStmts) {

        closeStatements(prepStmts);
        closeConnection(dbConnection);
    }

    public static void closeAllConnections(Connection dbConnection, ResultSet rs, PreparedStatement... prepStmts) {

        closeResultSet(rs);
        closeStatements(prepStmts);
        closeConnection(dbConnection);
    }

    public static void closeAllConnections(Connection dbConnection, ResultSet rs1, ResultSet rs2,
                                           PreparedStatement... prepStmts) {
        closeResultSet(rs1);
        closeResultSet(rs2);
        closeStatements(prepStmts);
        closeConnection(dbConnection);
    }

    /**
     * Retrieve connection to the database.
     *
     * @return {@link Connection}
     *
     */
    public static Connection getDBConnection() throws SQLException {
        Connection dbConnection = getJDBCDataSource().getConnection();
        dbConnection.setAutoCommit(false);
        if (dbConnection.getTransactionIsolation() != Connection.TRANSACTION_READ_COMMITTED) {
            dbConnection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        }
        return dbConnection;
    }


    // Loading JDBC data store on demand.
    private static DataSource getJDBCDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        String connectionUrl = "jdbc:h2:~/iot-fog/UM_DB";
        dataSource.setUrl(connectionUrl);
        dataSource.setDriverClassName("org.h2.Driver");
        return dataSource;
    }

    public static void updateUserRoleMappingInBatchMode(Connection dbConnection, String sqlStmt,
                                                        Object... params) throws Exception {
        PreparedStatement prepStmt = null;
        try {
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            int batchParamIndex = -1;
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    Object param = params[i];
                    if (param == null) {
                        throw new Exception("Null data provided.");
                    } else if (param instanceof String[]) {
                        batchParamIndex = i;
                    } else if (param instanceof String) {
                        prepStmt.setString(i + 1, (String) param);
                    } else if (param instanceof Integer) {
                        prepStmt.setInt(i + 1, (Integer) param);
                    }
                }
            }
            if (batchParamIndex != -1) {
                String[] values = (String[]) params[batchParamIndex];
                for (String value : values) {
                    prepStmt.setString(batchParamIndex + 1, value);
                    prepStmt.addBatch();
                }
            }

            int[] count = prepStmt.executeBatch();
            if (log.isDebugEnabled()) {
                log.debug("Executed a batch update. Query is : " + sqlStmt + ": and result is"
                          + Arrays.toString(count));
            }
            dbConnection.commit();
        } catch (SQLException e) {
            String errorMessage = "Using sql : " + sqlStmt + " " + e.getMessage();
            if (log.isDebugEnabled()) {
                log.debug(errorMessage, e);
            }
            throw new Exception(errorMessage, e);
        } finally {
            closeAllConnections(null, prepStmt);
        }
    }

    public static void createDefaultUserAndRole() throws UserManagementException {
        String checkCountSql = "SELECT COUNT(u.UM_ID) AS user_count, COUNT(r.UM_ID) AS role_count "
                + "FROM UM_USER u, UM_ROLE r;";

        Connection dbConnection = null;
        PreparedStatement prepStmt = null;

        try {
            dbConnection = getDBConnection();
            prepStmt = dbConnection.prepareStatement(checkCountSql);

            ResultSet rs = prepStmt.executeQuery();

            if(rs.next()) {
                UserManagementDAO userManagementDAO = UserManagementDAOFactory.getUserManagementDAO();

                if(rs.getInt("role_count") ==  0) {
                    userManagementDAO.addRole("admin", true);
                    userManagementDAO.addRole("user", true);
                    userManagementDAO.addRole("system", true);
                    log.info("Default roles added");
                }
                if(rs.getInt("user_count") == 0) {
                    User user = new User("admin", "1234");
                    Map<String, String> claims = new HashMap<>();
                    claims.put("fullName", "Admin");
                    user.setClaims(claims);
                    user.setRoles(new String[]{Constants.ADMIN_ROLE_NAME, Constants.USER_ROLE_NAME});
                    userManagementDAO.addUser(user);
                    log.info("Default admin added");

                    user = new User("system", "system");
                    user.setRoles(new String[]{"system"});
                    userManagementDAO.addUser(user);
                    log.info("System user added");
                }
            }

        } catch (Exception e) {
            throw new UserManagementException("Error occurred while adding default users", e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }
    }

    public static void addUserAttributes(Connection dbConnection, String userName, Map<String, String> properties)
            throws Exception {

        String sqlStmt = "INSERT INTO UM_USER_ATTRIBUTE (UM_USER_ID, UM_ATTR_NAME, UM_ATTR_VALUE, UM_PROFILE_ID) "
                + "VALUES ((SELECT UM_ID FROM UM_USER WHERE UM_USER_NAME=?), ?, ?, ?)";

        PreparedStatement prepStmt = null;
        boolean localConnection = false;

        try {
            if (dbConnection == null) {
                localConnection = true;
                dbConnection = DataBaseUtil.getDBConnection();
            }
            prepStmt = dbConnection.prepareStatement(sqlStmt);

            for (Map.Entry<String, String> entry : properties.entrySet()) {
                String propertyName = entry.getKey();
                String propertyValue = entry.getValue();
                CommonUtil.batchUpdateStringValuesToDatabase(prepStmt, userName, propertyName, propertyValue);
            }

            int[] counts = prepStmt.executeBatch();
            if (log.isDebugEnabled()) {
                int totalUpdated = 0;
                if (counts != null) {
                    for (int i : counts) {
                        totalUpdated += i;
                    }
                }

                if (totalUpdated == 0) {
                    log.debug("No rows were updated");
                }
                log.debug("Executed query is " + sqlStmt + " and number of updated rows :: " + totalUpdated);
            }

            if (localConnection) {
                dbConnection.commit();
            }
        } catch (SQLException e) {
            String msg = "Error occurred while updating string values to database.";
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new Exception(msg, e);
        } finally {
            if (localConnection) {
                DataBaseUtil.closeAllConnections(dbConnection);
            }
            DataBaseUtil.closeAllConnections(null, prepStmt);
        }
    }

    public static void updateUserAttributes(Connection dbConnection, String userName, Map<String, String> properties)
            throws Exception {

        String sqlStmt = "UPDATE UM_USER_ATTRIBUTE SET UM_ATTR_VALUE = ? "
                + "WHERE UM_ATTR_NAME = ? AND UM_USER_ID = (SELECT UM_ID FROM UM_USER WHERE UM_USER_NAME=?)";

        PreparedStatement prepStmt = null;
        boolean localConnection = false;

        try {
            if (dbConnection == null) {
                localConnection = true;
                dbConnection = DataBaseUtil.getDBConnection();
            }
            prepStmt = dbConnection.prepareStatement(sqlStmt);

            for (Map.Entry<String, String> entry : properties.entrySet()) {
                String propertyName = entry.getKey();
                String propertyValue = entry.getValue();
                CommonUtil.batchUpdateStringValuesToDatabase(prepStmt, propertyValue, propertyName, userName);
            }

            int[] counts = prepStmt.executeBatch();
            if (log.isDebugEnabled()) {
                int totalUpdated = 0;
                if (counts != null) {
                    for (int i : counts) {
                        totalUpdated += i;
                    }
                }

                if (totalUpdated == 0) {
                    log.debug("No rows were updated");
                }
                log.debug("Executed query is " + sqlStmt + " and number of updated rows :: " + totalUpdated);
            }

            if (localConnection) {
                dbConnection.commit();
            }
        } catch (SQLException e) {
            String msg = "Error occurred while updating string values to database.";
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new Exception(msg, e);
        } finally {
            if (localConnection) {
                DataBaseUtil.closeAllConnections(dbConnection);
            }
            DataBaseUtil.closeAllConnections(null, prepStmt);
        }
    }

    public static void removeUserRoleMapping(Connection dbConnection, String sqlStmt, String[] roles, String user)
            throws Exception {
        PreparedStatement prepStmt = null;
        boolean localConnection = false;

        try {
            if (dbConnection == null) {
                localConnection = true;
                dbConnection = DataBaseUtil.getDBConnection();
            }
            prepStmt = prepStmt = dbConnection.prepareStatement(sqlStmt);

            for (String role : roles) {
                CommonUtil.batchUpdateStringValuesToDatabase(prepStmt, role, user);
            }

            int[] counts = prepStmt.executeBatch();
            if (log.isDebugEnabled()) {
                int totalUpdated = 0;
                if (counts != null) {
                    for (int i : counts) {
                        totalUpdated += i;
                    }
                }

                if (totalUpdated == 0) {
                    log.debug("No rows were updated");
                }
                log.debug("Executed query is " + sqlStmt + " and number of updated rows :: " + totalUpdated);
            }

            if (localConnection) {
                dbConnection.commit();
            }
        } catch (SQLException e) {
            String msg = "Error occurred while removing user-role mappings from database.";
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new Exception(msg, e);
        } finally {
            if (localConnection) {
                DataBaseUtil.closeAllConnections(dbConnection);
            }
            DataBaseUtil.closeAllConnections(null, prepStmt);
        }
    }

    /**
     * Returns if a user with the given username is an admin user.
     *
     * @param username username
     * @return {@code true} if the user has the admin role.
     */
    public static boolean isAdminUser(String username) throws UserManagementException {

        String sqlStmt = "SELECT GROUP_CONCAT(r.UM_ROLE_NAME) AS roles "
                + "FROM UM_USER u, UM_USER_ROLE ur, UM_ROLE r "
                + "WHERE u.UM_ID = ur.UM_USER_ID AND ur.UM_ROLE_ID = r.UM_ID AND u.UM_USER_NAME = ?;";

        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        boolean localConnection = false;

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            if (dbConnection == null) {
                localConnection = true;
                dbConnection = DataBaseUtil.getDBConnection();
            }

            prepStmt = dbConnection.prepareStatement(sqlStmt);
            prepStmt.setString(1, username);

            ResultSet rs = prepStmt.executeQuery();

            if (rs.next()) {
                String roles = rs.getString("roles");
                return roles.contains("admin");
            }

        } catch (SQLException e) {
            throw new UserManagementException("Error occurred while role list of the user: " + username, e);
        } finally {
            if (localConnection) {
                DataBaseUtil.closeAllConnections(dbConnection);
            }
            DataBaseUtil.closeAllConnections(null, prepStmt);
        }
        return false;
    }
}
